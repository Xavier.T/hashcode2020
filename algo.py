from score import sortlib

def algo(context):    
    sortedLibraries = sortlib(context)
    return keepOnlyBestLibrary(sortedLibraries, context.dmax)

def keepOnlyBestLibrary(sortedLibraries, maxDays):
    length = len(sortedLibraries)
    if length > 0:
        finalResult = [sortedLibraries[0]]
        currentDay = sortedLibraries[0].signup_days
        for i in range(1, length):
            if(sortedLibraries[i].signup_days <= maxDays - currentDay):
                finalResult.append(sortedLibraries[i])
                currentDay += sortedLibraries[i].signup_days
        return finalResult
    else:
        print("Liste des meilleurs librairies vide !!!")        
