from library import Library

class context:

    def __init__(self,nbBooks, nbLibraries, dmax):
        self.nbBooks = 0
        self.dmax = 0
        self.librariesDict = {}
        self.libraries = []

        self.dmax = int(dmax)
        self.nbBooks = int(nbBooks)
        self.nbLibraries = int(nbLibraries)


    def addLibrary(self, library):
        self.librariesDict[library.id] = library
        self.libraries.append(library)
