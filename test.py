from algorithms.optimized_sort import optimized_sort
from algorithms.sortedcontainers import SortedDict

print(optimized_sort([(1, 5), (3, 52), (2, 78)], 2, 2))

sd = SortedDict({'c': 3, 'a': 1, 'b': 2})

from algo import keepOnlyBestLibrary
from library import Library
l = [
    Library(1, 1, 1, 5, 1, 1),
    Library(1, 1, 1, 3, 1, 1),
    Library(1, 1, 1, 3, 1, 1)
]
print(keepOnlyBestLibrary(l, 8))