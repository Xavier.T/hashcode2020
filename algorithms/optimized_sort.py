from heapq import heappush, heappop, heapify
from random import randrange

def optimized_sort(bookList, maxBook, libScanPerDay, addedBooks):
    tempList = []
    finalList = []
    length = len(bookList)
    for i in range(0, length):
        b = bookList[i]
        while(b in bookList)
            b = bookList[randrange(length)]
        tempList.append(convertToHeapq(b, libScanPerDay))

    heapify(tempList)
    size = 0
    while tempList and size < maxBook:
        item = heappop(tempList)
        size += 1
        finalList.append(getBookIdBack(item))

    return finalList


def convertToHeapq(element, libScanPerDay):
    bookId = element[0]
    bookScore = element[1]
    return (calculateOptimizedScore(bookScore, libScanPerDay), bookId)


def calculateOptimizedScore(score, libScanPerDay):
    return 1/(score/libScanPerDay)

def getBookIdBack(tuple):
    return tuple[1]