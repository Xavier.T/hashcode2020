from context import context
from library import Library
import statistics

def score(lib, dmax):
    nblivres_total = nb_noobk_in_context(dmax, lib)
    score = 0
    if (lib.nbBooks < nblivres_total):
        nblivres_total = lib.nbBooks
    score = lib.signup_days

    #print(score)
    return score

def nb_noobk_in_context(maxd, lib):
    return (maxd) * lib.nb_book_per_day


def sortlib(context):

    for e in context.libraries:
        e.score = score(e, context.dmax)

    return sorted(context.libraries, key=lambda Library: Library.score)

