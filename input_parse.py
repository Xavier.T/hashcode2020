from library import Library
from context import context

def read_input(filename) :

    input_folder = "inputs/";
    f = open(input_folder + filename, "r");
    firstline = f.readline().split()
    ctx = context(firstline[0], firstline[1], firstline[2])
    booksScores = f.readline().split()

    for i in range(ctx.nbLibraries):
        metaDataLibrary = f.readline().split()
        id = i
        nbBooks = int(metaDataLibrary[0])
        #print(nbBooks)
        signup_days = int(metaDataLibrary[1])
        nb_book_per_day = int(metaDataLibrary[2])
        currentBooks = f.readline().split()
        books_id = []
        books_score = {}
        for book in currentBooks:
            books_id.append(int(book))
            books_score[int(book)] = int(booksScores[int(book)])
        library = Library(id, nbBooks, nb_book_per_day, signup_days, books_score, books_id)
        ctx.addLibrary(library)

    
    return ctx
