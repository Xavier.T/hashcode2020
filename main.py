from input_parse import read_input
from algo import algo
from generate_shedule import main as generate_output

data = [
    'a_example.txt',
    'b_read_on.txt',
    'c_incunabula.txt',
    'e_so_many_books.txt',
    'f_libraries_of_the_world.txt',
    'd_tough_choices.txt',
]

score = 0
for d in data:
    i = read_input(d);
    #print(i.libraries)
    r = algo(i)
    s = generate_output(i.dmax, r, d)
    score += s

    # print(d)
    # for library in i.libraries:
    #     library.stat_score()
