from library import Library

def get_nb_libraries_to_signup(libraries, nb_days):
    nb_libraries_to_signup = 0
    days = 0
    for library in libraries:
        days += library.signup_days
        if days < nb_days:
            nb_libraries_to_signup += 1
    return nb_libraries_to_signup

def generate_output(libraries, nb_libraries_to_signup, deadline):
    score = 0
    date = 0
    output = ""

    output += str(nb_libraries_to_signup) + '\n'

    for library in libraries[:nb_libraries_to_signup]:

        nb_books_to_scan = library.get_nb_books(date, deadline)
        if nb_books_to_scan > library.nbBooks:
            nb_books_to_scan = library.nbBooks
        date += library.signup_days

        output += str(library.id) + ' ' + str(nb_books_to_scan) + '\n'

        for book_id in library.books_id[:nb_books_to_scan]:
            output += str(book_id) + ' '
            score += library.books_score[book_id]

        output += '\n'

    print(score)
    return output, score

def write_output(output, filename):
    output_folder = "outputs/";
    with open(output_folder + filename, 'w') as mf:
        mf.write(output)

def main(deadline, libraries, filename):
    nb_libraries_to_signup = get_nb_libraries_to_signup(libraries, deadline)

    r, s = generate_output(
        libraries[:nb_libraries_to_signup],
        nb_libraries_to_signup,
        deadline
    )
    write_output(r, filename)
    return s

if __name__ == '__main__':
    l = [
        Library(0, 5, 2, 2, {0: 1, 1: 2, 2: 3, 3: 6, 4: 5}, [0, 1, 2, 3, 4]),
        Library(1, 4, 1, 3, {3: 6, 3: 3, 5: 4, 0: 1}, [0, 2, 3, 5])
    ]

    # main(deadline, libraries)
    main(7, l)
