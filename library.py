class Library:
    def __init__(self, id, nbBooks, nb_book_per_day, signup_days, books_score, books_id):
        self.id = id
        self.signup_days = signup_days
        self.nb_book_per_day = nb_book_per_day
        self.books_score = books_score # dict { id: score }
        self.books_id = books_id # list [ id order by score ]
        self.nbBooks = nbBooks
        self.score = 0
    def get_score():
        pass

    def get_nb_books(self, signup_day, deadline):
        """Return the number of books the library can ship in n `days`"""
        nb_days = deadline - signup_day - self.signup_days
        nb_books = self.nb_book_per_day * nb_days
        return nb_books

    def stat_score(self):
        total = int()
        for book in self.books_score.keys():
            total += self.books_score[book]

        print('total')
        print(total)
        print('average')
        print(total / self.nbBooks)
